﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PiezaDerecha : MonoBehaviour {
	public float radio;
	public bool showRangeSphere = true;
	private Vector3 posUnionOtraPieza;
	private PiezaIzquierda piezaIzquierda;
	private PiezaDerecha piezaDerecha;
	private PiezaIzquierda otraPieza;
	private float shininess;
	private bool estaEnPunto=false;
	private bool estaCercaPieza=false;
	private float distAOtraPieza;
	private PuntoEncuentro puntoEncuentroMasCercano;
	private int signRot=1;
	private bool girado=false;
	// Use this for initialization
	void Start () { 
		piezaIzquierda = GameObject.Find ("PiezaIzquierda").GetComponent<PiezaIzquierda>();
		piezaDerecha = this;
		otraPieza = piezaIzquierda;
	}
	
	// Update is called once per frame
	void Update () { 
		if ((piezaDerecha.transform.position.x - piezaIzquierda.transform.position.x>0)  && !girado)
		{
			girado=true;
			rotarPiezas();
		}
		//if (Input.GetKeyDown (KeyCode.R))
		if ((piezaDerecha.transform.position.x - piezaIzquierda.transform.position.x<0)  && girado)
		{
			girado=false;
			rotarPiezas();
		} 

		 

		distAOtraPieza= Vector3.Distance(otraPieza.transform.position, transform.position);

		if (distAOtraPieza < radio) 
		{
			estaCercaPieza = true;
		} 
		else 
		{
			estaCercaPieza = false;
		}

		if (estaCercaPieza && !this.estaEnPuntoDeEncuentro() && otraPieza.estaEnPuntoDeEncuentro ()) 
		{
			getPuntoEncuentroMasCercano().llamarPiezas();
		}

		if (estaCercaPieza && !this.estaEnPuntoDeEncuentro () && !otraPieza.estaEnPuntoDeEncuentro ()) 
		{
			Vector3 direction=otraPieza.transform.position-transform.position;
			this.GetComponent<Rigidbody>().AddForce(-direction * 1200f*Time.deltaTime);
		}
 
	}

	public void rotarPiezas () { 
		signRot=-signRot;
		transform.Rotate(new Vector3(0,signRot*180,0));
		piezaIzquierda.transform.Rotate(new Vector3(0,signRot*180,0));
		
	}

	public void brillar () { 
		estaEnPunto = true;
		shininess = Mathf.PingPong (Time.time, 0.67f);
		this.GetComponent<Renderer> ().material.SetFloat ("_Shininess", shininess);
		
	}

	public void normalizar () { 
		estaEnPunto = false;
		shininess = 1.0f;
		this.GetComponent<Renderer>().material.SetFloat("_Shininess", shininess);
		
	}

	public bool estaEnPuntoDeEncuentro() { 
		return estaEnPunto; 
	}

	private PuntoEncuentro getPuntoEncuentroMasCercano() { 
		PuntoEncuentro puntoCercano = null;
		List<GameObject> listaPuntos = new List<GameObject>(GameObject.FindGameObjectsWithTag("PuntoEncuentro"));  
		float distancia = Mathf.Infinity;
		float distAPunto;
		foreach (GameObject punto in listaPuntos) 
		{
			distAPunto=Vector3.Distance(punto.transform.position, transform.position);
			if(distAPunto<distancia)
			{
				distancia=distAPunto;
				puntoCercano=punto.GetComponent<PuntoEncuentro>();
			}
		}
		return puntoCercano; 
	}

	void OnDrawGizmos() {
		if (showRangeSphere) {
			Gizmos.color =new Color (0,1,1,.5f);// Color.cyan;
			Gizmos.DrawSphere(transform.position, radio);
		}		
	}   
	 
}
