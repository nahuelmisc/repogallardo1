﻿using UnityEngine;
using System.Collections;

public class PuntoEncuentro : MonoBehaviour {
	public float radio;
	public bool showRangeSphere = true;
	private MovieTexture mvtxVideo;
	private PiezaIzquierda piezaIzquierda;
	private PiezaDerecha piezaDerecha;
	//Objetos del punto de encuentro
	private GameObject posUnionIzquierda;
	private GameObject posUnionDerecha;
	private GameObject videoPantalla;
	private GameObject videoEscala;
	private GameObject videoEscalaOriginal;
	//Distancias a las piezas
	private float distanceToLeft;
	private float distanceToRight;
	private bool cercaPiezaDerecha;
	private bool cercaPiezaIzquierda;

	private bool videoWasPlayed = false;
	private Transform transf;
	private Renderer renderer;
	private float shininess;
	// Use this for initialization
	void Start () {
		this.gameObject.tag = this.gameObject.name;

		piezaIzquierda = GameObject.Find ("PiezaIzquierda").transform.GetComponent<PiezaIzquierda>();
		piezaDerecha = GameObject.Find ("PiezaDerecha").transform.GetComponent<PiezaDerecha>();


		transf = this.gameObject.transform.Find("PosUnionIzquierda");
		if (transf != null)
		{
			posUnionIzquierda = transf.gameObject;
		}

		transf = this.gameObject.transform.Find("PosUnionDerecha");
		if (transf != null)
		{
			posUnionDerecha = transf.gameObject;
		}

		transf = this.gameObject.transform.Find("VideoEscala");
		if (transf != null)
		{
			videoEscala = transf.gameObject;
		} 

		transf = this.gameObject.transform.Find("VideoEscalaOriginal");
		if (transf != null)
		{
			videoEscalaOriginal = transf.gameObject;
		} 


		transf = this.gameObject.transform.Find("Video");
		if (transf != null)
		{
			videoPantalla = transf.gameObject;
			renderer = videoPantalla.GetComponent<Renderer>();
			renderer.enabled=false;
			mvtxVideo = (MovieTexture)renderer.material.mainTexture; 
			mvtxVideo.loop = false;
		} 

	}

	public void llamarPiezas () {
		iTween.MoveTo(piezaDerecha.gameObject, posUnionDerecha.transform.position, 1f);
		iTween.MoveTo(piezaIzquierda.gameObject, posUnionIzquierda.transform.position, 1f);
	}
	
	// Update is called once per frame
	void Update () { 
		if (Input.GetKeyDown (KeyCode.R))
		{
			//llamarPiezas (); 
		}

		distanceToLeft= Vector3.Distance(piezaIzquierda.gameObject.transform.position, transform.position);
		distanceToRight= Vector3.Distance(piezaDerecha.gameObject.transform.position, transform.position); 

		if (distanceToRight < radio) 
		{
			cercaPiezaDerecha = true;
		} 
		else 
		{
            cercaPiezaDerecha = false;
		}

        if (distanceToLeft < radio)
        {
            cercaPiezaIzquierda = true;
        }
        else
        {
            cercaPiezaIzquierda = false;
        }
        
		if (!cercaPiezaIzquierda && !cercaPiezaDerecha) 
		{
			piezaIzquierda.normalizar(); 
			piezaDerecha.normalizar(); 
		}

		if (cercaPiezaIzquierda && videoWasPlayed) 
		{
			piezaIzquierda.normalizar();
		}

		if (cercaPiezaDerecha && videoWasPlayed) 
		{
			piezaDerecha.normalizar();
		}




		if (!videoWasPlayed) {
			if (cercaPiezaDerecha && cercaPiezaIzquierda   )
			{
				unirPiezas(); 
				piezaIzquierda.normalizar();  
			}
			if (cercaPiezaIzquierda && !cercaPiezaDerecha) { 
				piezaIzquierda.brillar();
			}
			if (!cercaPiezaIzquierda && cercaPiezaDerecha) { 
				piezaDerecha.brillar();
			}
		}
		if (videoWasPlayed)
		{
			cercaPiezaDerecha=false;
			cercaPiezaDerecha=false;
		}

	}
	void unirPiezas() {
		iTween.MoveTo(piezaDerecha.gameObject, posUnionDerecha.transform.position, 1f);
		iTween.MoveTo(piezaIzquierda.gameObject, posUnionIzquierda.transform.position, 1f);
		desactivarMallas ();
		StartCoroutine(reproducirVideo ()); 
	}

	IEnumerator reproducirVideo() {
			
		videoPantalla.GetComponent<Renderer> ().enabled = true;
		mvtxVideo.Play();
		//escala videoPantalla a videoEscala
		iTween.ScaleTo (videoPantalla, iTween.Hash ("scale",videoEscala.transform.localScale , "time", 1.0f, "easetype", "easeInQuad"));
		//mueve videoPantalla a la posicion de videoEscala
		iTween.MoveTo(videoPantalla, videoEscala.transform.position, 8f);
		print (" mvtxVideo.duration "+mvtxVideo.duration);
		yield return new WaitForSeconds(mvtxVideo.duration);
		mvtxVideo.Stop();
		//escala videoPantalla a videoEscalaOriginal
		iTween.ScaleTo (videoPantalla, iTween.Hash ("scale",videoEscalaOriginal.transform.localScale , "time", 1.0f, "easetype", "easeInQuad"));
		//mueve videoEscala a la posicion de videoPantalla
		iTween.MoveTo(videoPantalla, videoEscalaOriginal.transform.position, 8f);
		videoPantalla.GetComponent<Renderer> ().enabled = false;
		videoWasPlayed = true;

	}

	void desactivarMallas() { 
		
	}

	void rechazarPiezas() { 
		
	}

	void OnDrawGizmos() {
		if (showRangeSphere) {
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(transform.position, radio);
		}

	}   
}
