﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {
	float speed=10.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.A)) 
		{ 
			this.GetComponent<Rigidbody>().AddForce(Vector3.left * speed *Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.D)) 
		{ 
			this.GetComponent<Rigidbody>().AddForce(Vector3.right * speed*Time.deltaTime);
		}
	}
}
