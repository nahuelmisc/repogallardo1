﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PiezaIzquierda : MonoBehaviour {
	public float radio;
	public bool showRangeSphere = true;
	private Vector3 posUnionOtraPieza;
	private PiezaIzquierda piezaIzquierda;
	private PiezaDerecha piezaDerecha;
	private PiezaDerecha otraPieza;
	private float shininess;
	private bool estaEnPunto=false;
	private bool estaCercaPieza=false;
	private float distAOtraPieza;
	private PuntoEncuentro puntoEncuentroMasCercano;
	// Use this for initialization
	void Start () { 
		piezaIzquierda = this;
		piezaDerecha = GameObject.Find ("PiezaDerecha").GetComponent<PiezaDerecha>();
		otraPieza = piezaDerecha;
	}
	
	// Update is called once per frame
	void Update () { 
		distAOtraPieza= Vector3.Distance(otraPieza.transform.position, transform.position);

		if (distAOtraPieza < radio) 
		{
			estaCercaPieza = true;
		} 
		else 
		{
			estaCercaPieza = false;
		}

		if (estaCercaPieza && !this.estaEnPuntoDeEncuentro() && otraPieza.estaEnPuntoDeEncuentro ()) 
		{
			getPuntoEncuentroMasCercano().llamarPiezas();
		}
		if (estaCercaPieza && !this.estaEnPuntoDeEncuentro () && !otraPieza.estaEnPuntoDeEncuentro ()) 
		{
			Vector3 direction=otraPieza.transform.position-transform.position;
			this.GetComponent<Rigidbody>().AddForce(-direction * 1200f*Time.deltaTime);
		}

		
	}

	public void brillar () { 
		estaEnPunto = true;
		shininess = Mathf.PingPong (Time.time, 0.67f);
		this.GetComponent<Renderer> ().material.SetFloat ("_Shininess", shininess);
		
	}

	public void normalizar () { 
		estaEnPunto = false;
		shininess = 1.0f;
		this.GetComponent<Renderer>().material.SetFloat("_Shininess", shininess);
		
	}

	public bool estaEnPuntoDeEncuentro() { 
		return estaEnPunto; 
	}

	private PuntoEncuentro getPuntoEncuentroMasCercano() { 
		PuntoEncuentro puntoCercano = null;
		List<GameObject> listaPuntos = new List<GameObject>(GameObject.FindGameObjectsWithTag("PuntoEncuentro"));  
		float distancia = Mathf.Infinity;
		float distAPunto;
		foreach (GameObject punto in listaPuntos) 
		{
			distAPunto=Vector3.Distance(punto.transform.position, transform.position);
			if(distAPunto<distancia)
			{
				distancia=distAPunto;
				puntoCercano=punto.GetComponent<PuntoEncuentro>();
			}
		}
		return puntoCercano; 
	}

	void OnDrawGizmos() {
		if (showRangeSphere) {
			Gizmos.color = new Color (1,0,0,.5f);//Color.red;
			Gizmos.DrawSphere(transform.position, radio);
		}
		
	}   
	
}