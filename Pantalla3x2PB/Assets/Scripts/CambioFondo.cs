﻿using UnityEngine;
using System.Collections;

public class CambioFondo : MonoBehaviour
{ 
	public MovieTexture loopNada;
	public MovieTexture trans1pA2p;
	public MovieTexture loop2P;
	public MovieTexture trans2pA1p;
	public MovieTexture anim1;
	public MovieTexture anim2;
	public MovieTexture anim3;
	
	private Renderer renderer;
	private MovieTexture movieTexture;
	private bool hay2Personas;
	private float tiempo;
	
	public static CambioFondo Instance = null;
	
	void Awake()
	{
		Instance = this;
	}
	
	
	
	// Use this for initialization
	void Start() 
	{
		tiempo = 0;
		renderer = GetComponent<Renderer>();
		movieTexture = (MovieTexture)renderer.material.mainTexture; 
		movieTexture.loop = false;
		movieTexture.Play();
		
		
	}
	
	// Update is called once per frame
	void Update()
	{
		adminTiempo ();
		
		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			
			print ("entraron 2");
			//movieTexture.Stop();  
			StartCoroutine(entran2Personas());
			
			
		}
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			
			print ("se va una persona");
			//movieTexture.Stop();  
			StartCoroutine(seVaUnaPersonaDeLasDos()); 
			
		}
		
	}
	
	IEnumerator seVaUnaPersonaDeLasDos()
	{
		hay2Personas = false;
		movieTexture = null;
		movieTexture = trans2pA1p;
		movieTexture.loop = false;
		renderer.material.mainTexture = movieTexture;
		movieTexture.Play();
		yield return new WaitForSeconds(trans2pA1p.duration);
		movieTexture.Stop();
		movieTexture = null;
		movieTexture = loopNada;
		movieTexture.loop = true;
		renderer.material.mainTexture = movieTexture;
		movieTexture.Play();
	}
	
	IEnumerator entran2Personas()
	{
		hay2Personas = true;
		movieTexture = null;
		movieTexture = trans1pA2p;
		movieTexture.loop = false;
		renderer.material.mainTexture=movieTexture;
		movieTexture.Play(); 
		yield return new WaitForSeconds(trans1pA2p.duration);
		movieTexture.Stop();
		movieTexture = null;
		movieTexture = loop2P;
		movieTexture.loop = true;
		renderer.material.mainTexture=movieTexture;
		movieTexture.Play();
		
		//aparecerFichas();
	}
	
	void adminTiempo()
	{
		tiempo += Time.deltaTime;
		
		if (tiempo > 8000) 
		{
			tiempo=0;
		}
	}
}
