﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class PanelInfo : MonoBehaviour { 
	public Sprite image1;
	public Sprite image2;
	public Sprite image3;
	public Sprite image4;
	public Sprite image5;
	public Sprite image6;
	public Sprite image7;
	public Sprite image8;
	public Sprite image9;
	public Sprite image10;
	public Sprite image11;
	public Sprite image12;
	public Sprite image13;
	public Sprite image14;
	public Sprite image15;
	public Sprite image16;
	public Sprite image17;
	public Sprite image18;
	public Sprite image19;
	public Sprite image20;

	private List<Sprite> listaSprites;
	private GameObject currentGO; 
	const string k_NormalTransitionName = "Normal";
	const string k_PressedTransitionName = "Pressed";
	const string k_DisabledStateName = "Disabled";
	const string k_HighlightedStateName = "Highlighted";

	private GameObject image;
	private Image imageSprite;
	private GameObject btnContinuar;
	// Use this for initialization
	void Start () {
		listaSprites = new List<Sprite> ();

		listaSprites.Add (image1);
		listaSprites.Add (image2);
		listaSprites.Add (image3);
		listaSprites.Add (image4);
		listaSprites.Add (image5);
		listaSprites.Add (image6);
		listaSprites.Add (image7);
		listaSprites.Add (image8);
		listaSprites.Add (image9);
		listaSprites.Add (image10);
		listaSprites.Add (image11);
		listaSprites.Add (image12);
		listaSprites.Add (image13);
		listaSprites.Add (image14);
		listaSprites.Add (image15);
		listaSprites.Add (image16);
		listaSprites.Add (image17);
		listaSprites.Add (image18);
		listaSprites.Add (image19);
		listaSprites.Add (image20);





		Transform transf;
		currentGO = GameObject.Find(gameObject.name);
		
		transf = currentGO.transform.Find("ImageInfo");
		if (transf != null)
		{ 
			image = transf.gameObject;  
			imageSprite=image.GetComponent<Image>();
		}

		transf = currentGO.transform.Find("btnContinuar");
		if (transf != null)
		{ 
			btnContinuar = transf.gameObject;  
			btnContinuar.GetComponent<Button>().onClick.AddListener(() => btnContinuarClick());
		}
	
	}
	private void btnContinuarClick()
	{ 
		this.GetComponent<Animator>().SetBool(k_NormalTransitionName, true);

		//Resetear juego si corresponde
		if(PanelCartas.Instance.todasLasCartasYaFueronUnidas())
		{
			PanelCartas.Instance.resetJuego();
		}
 
	}
	//El idEspecie de las 20 especies del juego
	public void cambiarImagen(int idEspecie)
	{	
		idEspecie--; 
		int height = listaSprites.ElementAt(idEspecie).texture.height;
		//int width = listaSprites.ElementAt(rand).texture.width;
		int width = (int)imageSprite.minWidth;
		//FALTA AJUSTAR EL ANCHO AL ORIGINAL DEL IMAGE Y RESETEAR EL SCROLL
		imageSprite.sprite = listaSprites.ElementAt(idEspecie);
		//resetScroll();
		//imageSprite.rectTransform.sizeDelta=new Vector2(width,height);

	}
	// Update is called once per frame
	void Update () {
		
	}

}
