﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class PanelCartas : MonoBehaviour
{
	public static PanelCartas Instance = null;
	public Texture texturaBrillo;
    private GameObject currentGO;
    const string k_NormalTransitionName = "Normal";
    const string k_PressedTransitionName = "Pressed";
    const string k_DisabledStateName = "Disabled";
    const string k_HighlightedStateName = "Highlighted";
	private GameObject panelInfo;

	#region Imagenes  
	public Sprite image1;
	public Sprite image2;
	public Sprite image3;
	public Sprite image4;
	public Sprite image5;
	public Sprite image6;
	public Sprite image7;
	public Sprite image8;
	public Sprite image9;
	public Sprite image10;
	public Sprite image11;
	public Sprite image12;
	public Sprite image13;
	public Sprite image14;
	public Sprite image15;
	public Sprite image16;
	public Sprite image17;
	public Sprite image18;
	public Sprite image19;
	public Sprite image20;
	#endregion

    #region Cartas
	private GameObject carta0;
	private GameObject carta1;
	private GameObject carta2;
	private GameObject carta3;
	private GameObject carta4;
	private GameObject carta5;
	private GameObject carta6;
	private GameObject carta7;
	private GameObject carta8;
	private GameObject carta9;
	private GameObject carta10;
	private GameObject carta11;
	private GameObject carta12;
	private GameObject carta13;
	private GameObject carta14;
	private GameObject carta15;
    #endregion
    List<GameObject> listaCartas;
	List<Sprite> listaSprites;

	void Awake()
	{
		Instance = this;
	}

    // Use this for initialization
    void Start()
    {
		listaCartas = new List<GameObject>();
		listaSprites = new List<Sprite>();

		listaSprites.Add (image1);
		listaSprites.Add (image2);
		listaSprites.Add (image3);
		listaSprites.Add (image4);
		listaSprites.Add (image5);
		listaSprites.Add (image6);
		listaSprites.Add (image7);
		listaSprites.Add (image8);
		listaSprites.Add (image9);
		listaSprites.Add (image10);
		listaSprites.Add (image11);
		listaSprites.Add (image12);
		listaSprites.Add (image13);
		listaSprites.Add (image14);
		listaSprites.Add (image15);
		listaSprites.Add (image16);
		listaSprites.Add (image17);
		listaSprites.Add (image18);
		listaSprites.Add (image19);
		listaSprites.Add (image20);
		
		
		
		Transform transf;
        currentGO = GameObject.Find(gameObject.name);

		transf = currentGO.transform.Find("PanelInfo");
		if (transf != null)
		{ 
			panelInfo = transf.gameObject;  
		}
		
		//Busqueda
		for (int i = 1; i < 17; i++)
		{
			string indStr = i.ToString("00");
            transf = currentGO.transform.Find("Carta" + indStr);
            if (transf != null)
            {
                GameObject carta;
                carta = transf.gameObject;
				carta.transform.Find("Img").GetComponent<Image>().material.color=hexStringToColor("ffffff");
                listaCartas.Add(carta);
            }
        }

        //Asignar clicks
        foreach (GameObject carta in listaCartas)
        {
            int index = listaCartas.IndexOf(carta);

            AddListener(carta, index);

        }
    }

    void AddListener(GameObject b, int value)
    {
        b.GetComponent<Button>().onClick.AddListener(() => seleccionarCarta(value));
    }

    private void seleccionarCarta(int value)
    {  
        GameObject selectedButton = listaCartas.ElementAt(value);
        if (!selectedButton.GetComponent<Figura>().yaEstaUnida())
        {
            selectedButton.GetComponent<Figura>().seleccionar();
            selectedButton.GetComponent<Animator>().SetBool(k_HighlightedStateName, true); 

            if (getCantSelected() > 1)
            {
                if (coincidenFiguras())
                {
					StartCoroutine(brillar());
					showPanel();
                    unirFiguras();  
                }
                else
                {
                    resetCartas();
                }

            }
        }


    }
	private void showPanel()
	{ 
		cambiarImagenPanel ();
		panelInfo.GetComponent<Animator>().SetBool(k_HighlightedStateName, true); 
	}

	private void cambiarImagenPanel()
	{
		List<GameObject> listaSelected = getCartasSelected();
		//Recupera el indice del nombre de la figura
		int index = int.Parse (listaSelected.ElementAt (0).transform.Find("Img").GetComponent<Image>().sprite.name);
		panelInfo.GetComponent<PanelInfo> ().cambiarImagen (index);
		 
	}

	private void unirFiguras()
	{
		List<GameObject> listaSelected = getCartasSelected();
        foreach (GameObject g in listaSelected)
        {
            Figura figura = g.GetComponent<Figura>();
            figura.unirFigura(); 
        }
    }

    private bool coincidenFiguras()
    {
        List<GameObject> listaSelected = getCartasSelected();
        string sprFigura1 = listaSelected.ElementAt(0).transform.Find("Img").GetComponent<Image>().sprite.name;
        string sprFigura2 = listaSelected.ElementAt(1).transform.Find("Img").GetComponent<Image>().sprite.name;
        if (sprFigura1 == sprFigura2)
        {
            return true;
        }
        else
        {
            return false;
        } 
    }

    private List<GameObject> getCartasSelected()
    {
        List<GameObject> listaSelected = new List<GameObject>();
        foreach (GameObject g in listaCartas)
        {
            Figura figura = g.GetComponent<Figura>();
            if (!figura.yaEstaUnida() && figura.estaSeleccionada())
            {
                listaSelected.Add(g);
            }
        }
        if (listaSelected.Count != 2)
        {
            Debug.LogError("getCartasSelected: Tiene que devolver 2 elementos seleccionados");
        }
        return listaSelected;
    }

	private int getCantCartasUnidas()
	{ 
		int cantCartasUnidas=0;
		foreach (GameObject g in listaCartas)
		{
			Figura figura = g.GetComponent<Figura>();
			if (figura.yaEstaUnida())
			{
				cantCartasUnidas++;
			}
		} 
		return cantCartasUnidas;
	}

	public bool todasLasCartasYaFueronUnidas()
	{ 
		int cantCartasUnidas=getCantCartasUnidas();
		if (cantCartasUnidas == listaCartas.Count) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}
 
    public void resetJuego()
    { 
		sortearCartas ();
		resetAllCartas ();
    }
	private void sortearCartas()
	{ 
		/*TERMINAR*/
		int random;
		List<int> elegidos= null;
		elegidos= new List<int>();

		for (int i=0; i<8; i++) 
		{ 
			bool continuar;
			do{
				continuar=false;
				random=Random.Range(0,20);
				if(yaEstaEnLista(random,elegidos))
				{
					continuar=false;
				}
				else
				{
					continuar=true;
				}
			}while(!continuar); 			 
			elegidos.Add(random); 
		}

		foreach (int ent in elegidos)
		{
			print (elegidos.IndexOf(ent)+") "+ent);
		}

		//Resetear Texturas
		List<int> mapa= null;
		mapa= new List<int>();

		int pos1,pos2;
		for (int i=0; i<8; i++) 
		{ 
			bool continuar;
			do{
				continuar=false;
				pos1=Random.Range(0,16);
				if(yaEstaEnLista(pos1,mapa))
				{
					continuar=false;
				}
				else
				{
					continuar=true;
				}
			}while(!continuar); 
			mapa.Add(pos1); 
			bool continuar2;
			do{
				continuar2=false;
				pos2=Random.Range(0,16);
				if(yaEstaEnLista(pos2,mapa))
				{
					continuar2=false;
				}
				else
				{
					continuar2=true;
				}
			}while(!continuar2); 
			mapa.Add(pos2);

			listaCartas.ElementAt(pos1).transform.Find("Img").GetComponent<Image>().sprite=listaSprites.ElementAt(elegidos.ElementAt(i));
			listaCartas.ElementAt(pos2).transform.Find("Img").GetComponent<Image>().sprite=listaSprites.ElementAt(elegidos.ElementAt(i));

		}
 
	}

	private bool yaEstaEnLista(int num, List<int> mapa)
	{
		if (mapa.Count == 0) {
			return false;
		}
		foreach (int ent in mapa)
		{
			if(ent==num)
			{
				return true;
			}
		}
		return false;
	}

    private void resetCartas()
    {
        foreach (GameObject b in listaCartas)
        {
            Figura figura = b.GetComponent<Figura>();
            if (figura.estaSeleccionada())
            {
                b.GetComponent<Animator>().SetBool(k_DisabledStateName, true);
                figura.deseleccionar();
            }
        }
    }

	IEnumerator brillar()
	{
		List<GameObject> listaSelected = getCartasSelected(); 
		foreach (GameObject gs in listaSelected) 
		{ 
			gs.transform.Find("Img").GetComponent<Image>().material.color=hexStringToColor("f1f287");
			yield return new WaitForSeconds(0.05f);
			gs.transform.Find("Img").GetComponent<Image>().material.color=hexStringToColor("ffffff");
		}
		 
	}

	private void resetAllCartas()
	{
		foreach (GameObject b in listaCartas)
		{
			Figura figura = b.GetComponent<Figura>();
			figura.deseleccionar ();
			figura.desunir();
			b.GetComponent<Animator>().SetBool(k_DisabledStateName, true);
			 
		}
	}

    private int getCantSelected()
    {
        int cantSel = 0;
        foreach (GameObject b in listaCartas)
        {
            Figura figura = b.GetComponent<Figura>();
            if (figura.estaSeleccionada())
            {
                cantSel++;
            }
        }
        return cantSel;
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown (KeyCode.R)) 
		{
			resetJuego();
		}
		if (Input.GetKeyDown (KeyCode.B)) 
		{
			brillar();
		}

    }
	public Color32 hexStringToColor(string hex)
	{
		//Example F54F29
		byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
		return new Color32(r, g, b, 255);
	}


}
