﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Figura : MonoBehaviour
{
	//Nro de Figura
	private int nroFigura;
	//Sprite
	private Sprite sprite;
    //Si se selecciono
    private bool isSelected=false;
    //Si ya se unio con otra
    private bool isMatched=false;

    public bool yaEstaUnida()
    {
        return isMatched;
    }

    public bool estaSeleccionada()
    {
        return isSelected;
    }
    public void seleccionar()
    {
        if (!isSelected && !isMatched)
        {
            isSelected = true;
        }
    }
	public void deseleccionar()
	{
		isSelected = false;
	}
	public void desunir()
	{
		isMatched = false;
	}
    public void unirFigura()
    {
        if (!isMatched)
        {
            isSelected = false;
            isMatched = true;
        }
    }

    // Use this for initialization
    void Start()
    {
		sprite = this.gameObject.GetComponent<Image> ().sprite;
		isMatched=false;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
