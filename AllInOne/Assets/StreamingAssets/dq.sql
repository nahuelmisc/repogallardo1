CREATE TABLE cache (
    "id" INTEGER NOT NULL,
    "module" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "url" TEXT NOT NULL,
    "localfile" TEXT,
    "size" INTEGER,
    "hash" TEXT,
    "date" TEXT,
    "json" TEXT,
    "type" TEXT NOT NULL DEFAULT ('undefined')
, "idx" INTEGER  NOT NULL  DEFAULT (0));
