﻿using UnityEngine;
using System.Collections;

namespace FloorPlant  {
	
	public class FloorPlantStruct
	{
		
		public class Imagen {
			public int 		index;
			public int 		id;
			public string	titulo;
			public string	archivo;
			public long		size;
			public string	hash;
			
		}
		
		public class Archivo {
			public int 		index;
			public int 		id;
			public string	titulo;
			public string	archivo;
			public long		size;
			public string	hash;
			
		}
		
		public class Video {
			public int 		index;
			public int		id;
			public string	url;
			public string	idvideo;
			public string	servicio;
			public string	titulo;
			public string	archivo;
			public long		size;
			public string	hash;
			
		}
		
		public class FloorPlant {
			public int		id;
			public string	titulo;
			public string	resumen;
			public string	icono;
			public string	imagemap;
			public string	texto;
			public string	fecham;
			public string	blockcolor;
			public Imagen[]	imagen;
			public Video[]	video;
			
		}
		
		public class FloorPlantResponseData {
			public int 		codigo;
			public string	estado;
			public int		cantidad;
			public int		total;
			public int      offset;
			public FloorPlant[]	datos;
		}
		
		public class FloorPlantWSResponse {
			public 		FloorPlantResponseData Response;
			
		}
		
		public class FloorPlantService : ServiceHandler {
			public FloorPlantService() {
				serviceType= typeof(FloorPlantWSResponse);
				serviceName = "FLOORPLANT";
				url = "floorplant";
			}
			public new FloorPlantWSResponse getQueryResult() {
				return ((FloorPlantWSResponse)queryResult);
			}
			public void loadFromCache() {
				setQueryResult(PlayerPrefs.GetString (serviceType.Name));
			}
			
		}
	}
	
}
