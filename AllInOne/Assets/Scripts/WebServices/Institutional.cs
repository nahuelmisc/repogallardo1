﻿using UnityEngine;
using System.Collections;

namespace Institutional  {
	
	public class InstitutionalStruct
	{
		public class Imagen {
			public int 		index;
			public int 		id;
			public string	titulo;
			public string	archivo;
			public long		size;
			public string	hash;
			
		}
		
		public class Archivo {
			public int 		index;
			public int 		id;
			public string	titulo;
			public string	archivo;
			public long		size;
			public string	hash;
			
		}
		
		public class Video {
			public int 		index;
			public int		id;
			public string	url;
			public string	idvideo;
			public string	servicio;
			public string	titulo;
			public string	archivo;
			public long		size;
			public string	hash;
			
		}

		public class Institutional {
			public int		id;
			public string	titulo;
			public string	url;
			public string	texto;
			public string	fecham;
			public string	blockcolor;
			public Imagen[]	imagen;
			public Video[]	video;
			public Archivo[]	archivo;
			
		}
		
		public class InstitutionalResponseData {
			public int 		codigo;
			public string	estado;
			//public int		cantidad;
			public int		total;
			public int      offset;
			public Institutional[]	datos;
		}
		
		public class InstitutionalWSResponse {
			public 		InstitutionalResponseData Response;
			
		}
		
		public class InstitutionalService : ServiceHandler {
			public InstitutionalService() {
				serviceType= typeof(InstitutionalWSResponse);
				serviceName = "INSTITUTIONAL";
				url = "institucional";
			}
			public new InstitutionalWSResponse getQueryResult() {
				return ((InstitutionalWSResponse)queryResult);
			}
			public void loadFromCache() {
				setQueryResult(PlayerPrefs.GetString (serviceType.Name));
			}
			
		}
	}
	
}

