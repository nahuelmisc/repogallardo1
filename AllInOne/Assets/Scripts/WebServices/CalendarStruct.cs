﻿using UnityEngine;
using System.Collections;

namespace Calendar  {
	
	public class CalendarStruct
	{

		public class Imagen {
			public int 		index;
			public int 		id;
			public string	titulo;
			public string	archivo;
			public long		size;
			public string	hash;

		}

		public class Archivo {
			public int 		index;
			public int 		id;
			public string	titulo;
			public string	archivo;
			public long		size;
			public string	hash;
			
		}

		public class Video {
			public int 		index;
			public int		id;
			public string	url;
			public string	idvideo;
			public string	servicio;
			public string	titulo;
			public string	archivo;
			public long		size;
			public string	hash;
			
		}


		public class Calendar {
			public int		id;
			public string	titulo;
			public string	resumen;
			public string	url;
			public string	fechaactividad;
			public string	fecha;
			public string	fecham;
			public string	texto;
			public string	blockcolor;
			public Imagen[]	imagen;
			public Video[]	video;
			public Archivo[] archivo;
			
		}
		
		public class CalendarResponseData {
			public int 		codigo;
			public string	estado;
			public int		cantidad;
			public int		total;
			public int      offset;
			public Calendar[]	datos;
		}
		
		public class CalendarWSResponse {
			public 		CalendarResponseData Response;
			
		}
		
		public class CalendarService : ServiceHandler {
			public CalendarService() {
				serviceType= typeof(CalendarWSResponse);
				serviceName = "CALENDAR";
				url = "actividades";
			}
			public new CalendarWSResponse getQueryResult() {
				return ((CalendarWSResponse)queryResult);
			}
			public void loadFromCache() {
				setQueryResult(PlayerPrefs.GetString (serviceType.Name));
			}
			
		}
	}
	
}
