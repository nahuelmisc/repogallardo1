using UnityEngine;
using System.Collections;

namespace Promo  {

	public class PromoStruct
	{

		public class Promo {
			public int		id;
			public string	titulo;
			public string	texto;
			public string	archivo;
			public string	url;
			public int		size;
			public string	hash;
			public string	fecham;
			
		}
		
		public class PromoResponseData {
			public int 		codigo;
			public string	estado;
			public int		total;
			public Promo[]	datos;
		}
		
		public class PromoWSResponse {
			public 		PromoResponseData Response;
			
		}

		public class PromoService : ServiceHandler {
			public PromoService() {
				serviceType= typeof(PromoStruct.PromoWSResponse);
				serviceName = "PROMO";
				url = "promo";
			}
			public new PromoStruct.PromoWSResponse getQueryResult() {
				return ((PromoStruct.PromoWSResponse)queryResult);
			}
			public void loadFromCache() {
				setQueryResult(PlayerPrefs.GetString (serviceType.Name));
			}

		}
	}

}