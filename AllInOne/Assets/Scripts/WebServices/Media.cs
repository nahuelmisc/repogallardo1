using UnityEngine;
using System.Collections;

public class Media
{
	/*
image/jpeg
image/png
audio/mpeg
video/mp4
	 * */

	public string id;
	public string name;
	public string type;
	public string url;
	public string service;
	public bool isLocal;
	public int index;
	public long size;
	public string hash;
	public long fileId;
	public long contentId;

	public Media() {
		type = "";
	}
}

