using UnityEngine;
using System.Collections;

public enum WSStatus {
	IDLE,
	ON_REQUEST,
	FAILED,
	RESPONSE_OK
};

