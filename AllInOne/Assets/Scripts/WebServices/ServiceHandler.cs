using UnityEngine;
using System;
using System.Collections;
using Promo;
using Calendar;
using FloorPlant;
using Institutional;
using Pathfinding.Serialization.JsonFx;
using System.Net;
using System.IO;

public class ServiceHandler
{
	public int					retries;
	public WSStatus 			status;
	public bool 				queryInprogress = false;
	public Type 				serviceType;
	public string 				serviceName;
	protected object			queryResult;
	public string				url;
	public string 				lastError;
	
	private bool _cacheEnabled;
	
	public bool CacheEnabled
	{
		get { return this._cacheEnabled; }
		set { this._cacheEnabled = value; }
	}
	
	public bool isReady() {
		return !queryInprogress;
	}
	
	public void reset() {
		retries = 0;
		queryInprogress = false;
		status = WSStatus.IDLE;
	}
	
	public WSStatus getStatus() {
		return status;
	}
	
	public void setQueryResult(string parseText) {
		
		if ((parseText!=null) && (parseText.Length>0)) { //valid string
			switch (serviceName) { 
			case "PROMO":
				queryResult = JsonReader.Deserialize<PromoStruct.PromoWSResponse> (parseText);
				break;
			case "CALENDAR":
				queryResult = JsonReader.Deserialize<CalendarStruct.CalendarWSResponse> (parseText);
				break;
			case "FLOORPLANT":
				queryResult = JsonReader.Deserialize<FloorPlantStruct.FloorPlantWSResponse> (parseText);
				break;
			case "INSTITUTIONAL":
				queryResult = JsonReader.Deserialize<InstitutionalStruct.InstitutionalWSResponse> (parseText);
				break;
			default:
				Debug.Log ("Ouch!");
				break;
			}
			
			PlayerPrefs.SetString (serviceType.Name, parseText); //save JSON
		} else
			queryResult = null;
	}
	
	public object getQueryResult() {
		return queryResult;
	}
	
	
	public void queueServiceCall() {
		if (isReady()) {
			Debug.Log("Query process in queue (" + serviceName + ")...");
			reset();
			queryInprogress = true;
		}
		//Debug.Log("status=" + status + ", queryInprogress=" + queryInprogress);
	}
	
	
	public void saveCache() {
		if (status != WSStatus.RESPONSE_OK)
			return;
		
		if (!_cacheEnabled)
			return;
		
		WebClient webClient = new WebClient ();
		
		switch (serviceName) { 
		case "PROMO":
			PromoStruct.PromoWSResponse rs = queryResult as PromoStruct.PromoWSResponse;
			for (int i=0; i<rs.Response.datos.Length; i++) {
				long id = rs.Response.datos [i].id;
				string module = serviceName;
				string type = WebServiceHandler.getMIMEType(rs.Response.datos [i].archivo);
				long content = id;
				
				WebServiceHandler.CacheResult cr = WebServiceHandler.Instance.getCache(id, content, module, type, rs.Response.datos [i].hash);
				string localFile = WebServiceHandler.Instance.getFileCacheName(id, content, module, type, rs.Response.datos [i].hash);
				string fileName = rs.Response.datos [i].archivo;
				
				if (!cr.cached) {
					string url = "/uploadsfotos/" + fileName;
					Debug.Log ("Downloading '" + url + "' as '" + localFile + "'...");
					
					try {
						webClient.DownloadFile (WebServiceHandler.selectedServer + url, localFile);
						
						string name = fileName;
						int order = i;
						long size = rs.Response.datos [i].size;
						string hash = rs.Response.datos [i].hash;
						WebServiceHandler.Instance.registerCachedFile(cr, id, content, module, type, name, url, localFile, order, size, hash);
						
					} catch (Exception e)  { 
						Debug.LogError("PROMO Cache error: " + e.Message);
						
					}
				}
			}
			break;
			
		case "CALENDAR":
			CalendarStruct.CalendarWSResponse cs = queryResult as CalendarStruct.CalendarWSResponse;
			
			for (int i=0; i<cs.Response.datos.Length; i++) {
				long content = cs.Response.datos [i].id;
				
				CalendarStruct.Imagen[] imgs = cs.Response.datos [i].imagen;
				for (int j=0; j<imgs.Length; j++) {
					long id = imgs[j].id;
					string module = serviceName;
					string type = WebServiceHandler.getMIMEType(imgs[j].archivo);
					string fileName = imgs[j].archivo.Replace("/uploadsfotos/","");
					
					WebServiceHandler.CacheResult cr = WebServiceHandler.Instance.getCache(id, content, module, type, imgs[j].hash);
					string localFile = WebServiceHandler.Instance.getFileCacheName(id, content, module, type, imgs[j].hash);
					
					if (!cr.cached) {
						string url = imgs[j].archivo;
						Debug.Log ("Downloading '" + url + "' as '" + localFile + "' in folder " + WebServiceHandler.Instance.getCacheFolder());
						
						try {
							webClient.DownloadFile (WebServiceHandler.selectedServer + url, localFile);
							
							string name = fileName;
							int order = j;
							long size = imgs[j].size;
							string hash = imgs[j].hash;
							WebServiceHandler.Instance.registerCachedFile(cr, id, content, module, type, name, url, localFile, order, size, hash);
						} catch (Exception e)  { 
							Debug.LogError("CALENDAR Cache error: " + e.Message);
							
						}
					}
				}
			}
			
			break;
			
		case "FLOORPLANT":
			FloorPlantStruct.FloorPlantWSResponse fp = queryResult as FloorPlantStruct.FloorPlantWSResponse;
			
			for (int i=0; i<fp.Response.datos.Length; i++) {
				long content = fp.Response.datos [i].id;
				
				FloorPlantStruct.Imagen[] imgs = fp.Response.datos [i].imagen;
				for (int j=0; j<imgs.Length; j++) {
					long id = imgs[j].id;
					string module = serviceName;
					string type = WebServiceHandler.getMIMEType(imgs[j].archivo);
					string fileName = imgs[j].archivo.Replace("/uploadsfotos/","");
					
					WebServiceHandler.CacheResult cr = WebServiceHandler.Instance.getCache(id, content, module, type, imgs[j].hash);
					string localFile = WebServiceHandler.Instance.getFileCacheName(id, content, module, type, imgs[j].hash);
					
					if (!cr.cached) {
						string url = imgs[j].archivo;
						Debug.Log ("Downloading '" + url + "' as '" + localFile + "' in folder " + WebServiceHandler.Instance.getCacheFolder());
						
						try {
							webClient.DownloadFile (WebServiceHandler.selectedServer + url, localFile);
							
							string name = fileName;
							int order = j;
							long size = imgs[j].size;
							string hash = imgs[j].hash;
							WebServiceHandler.Instance.registerCachedFile(cr, id, content, module, type, name, url, localFile, order, size, hash);
						} catch (Exception e)  { 
							Debug.LogError("FLOORPLANT Cache error: " + e.Message);
							
						}
					}
				}
			}
			
			break;
			
		case "INSTITUTIONAL":
			InstitutionalStruct.InstitutionalWSResponse ins = queryResult as InstitutionalStruct.InstitutionalWSResponse;
			
			for (int i=0; i<ins.Response.datos.Length; i++) {
				long content = ins.Response.datos [i].id;
				
				InstitutionalStruct.Imagen[] imgs = ins.Response.datos [i].imagen;
				for (int j=0; j<imgs.Length; j++) {
					long id = imgs[j].id;
					string module = serviceName;
					string type = WebServiceHandler.getMIMEType(imgs[j].archivo);
					string fileName = imgs[j].archivo.Replace("/uploadsfotos/","");
					
					WebServiceHandler.CacheResult cr = WebServiceHandler.Instance.getCache(id, content, module, type, imgs[j].hash);
					string localFile = WebServiceHandler.Instance.getFileCacheName(id, content, module, type, imgs[j].hash);
					
					if (!cr.cached) {
						string url = imgs[j].archivo;
						Debug.Log ("Downloading '" + url + "' as '" + localFile + "' in folder " + WebServiceHandler.Instance.getCacheFolder());
						
						try {
							webClient.DownloadFile (WebServiceHandler.selectedServer + url, localFile);
							
							string name = fileName;
							int order = j;
							long size = imgs[j].size;
							string hash = imgs[j].hash;
							WebServiceHandler.Instance.registerCachedFile(cr, id, content, module, type, name, url, localFile, order, size, hash);
						} catch (Exception e)  { 
							Debug.LogError("FLOORPLANT Cache error: " + e.Message);
							
						}
					}
				}
			}
			
			break;
		}
	}
}

