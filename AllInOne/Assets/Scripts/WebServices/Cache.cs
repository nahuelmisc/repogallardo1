using UnityEngine;
using System.Collections;
using SQLite4Unity3d;
using System;

public class Cache {
	[PrimaryKey, AutoIncrement]
	public int id { get; set; }
	public string module { get; set; }
	public string type { get; set; }
	public int idx { get; set; }
	public int content { get; set; }
	public string name { get; set; }
	public string url { get; set; }
	public string localfile { get; set; }
	public int order { get; set; }
	public int size { get; set; }
	public string hash { get; set; }
	public string cachedate { get; set; }
	public string json { get; set; }
	public string aux { get; set; }
	
	public override string ToString ()
	{
		return string.Format ("[Cache: id={0}, module={1}, type={2}, idx={3}, content={4}, name={5}, url={6}, localfile={7}, order={8}, size={9}, hash={10}, cachedate={11}, json={12}, aux={13}]", id, module, type, idx, content, name ,url, localfile, order, size, hash, cachedate, json, aux);
	}
}


