﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;

public class PanelBloque : MonoBehaviour {
	private GameObject currentGO;
	private Image sprImagen;
    private Text txtTexto;
    private Text txtTitulo; 
    
	private float imgHeight;
	private float txtHeight;
    private float titHeight;
	private float imgWidth;
    private float txtWidth;
    private float titWidth;
	private float offSet=160.0f;
	private bool actualizoBloque=false;
    private Vector3 escalaZero;
	private Vector3 escalaNormal;

#region Properties

    private Sprite _Imagen;
    public Sprite Imagen
    {
        get
        {
            return _Imagen;
        }
        set
        {
            _Imagen = value; 
        }
    }

    private string _Texto; 
    public string Texto
    {
        get
        {
            return _Texto;
        }
        set
        {
            _Texto = value;
           
            
        }
    }

    private string _Titulo;
    public string Titulo
    {
        get
        {
            return _Titulo;
        }
        set
        {
            _Titulo = value;


        }
    }

	private float _Alto;
	public float Alto
	{
		get 
		{  
			_Alto= imgHeight + txtHeight + titHeight + offSet;
			return _Alto; 
		}
		set 
		{ 
			_Alto = value;  
			RectTransform rtThis=this.GetComponent<RectTransform>();
			float ancho=rtThis.rect.width;
			rtThis.sizeDelta = new Vector2( ancho, _Alto);
		}
	}

	private float _Ancho;
	public float Ancho
	{
        //El ancho no cambia
		get {
            RectTransform rt = this.GetComponent<RectTransform>();
			_Ancho=rt.rect.width;
			return _Ancho; 
		}
		set { 
			_Ancho = value; 
			RectTransform rtThis = this.GetComponent<RectTransform>(); 
			rtThis.sizeDelta = new Vector2( _Ancho, _Alto);
		}
	}

	private bool _Visible;
	public bool Visible
	{
		get { 
			return _Visible; 
		}
		set { 
			_Visible = value;
			if(_Visible)
			{
                actualizarBloque(); 
			}
			else
			{
				this.transform.localScale = escalaZero;
			}
		}
	}

#endregion

    // Use this for initialization
	void Start () {
		Transform transf;
		
		currentGO = GameObject.Find(gameObject.name); 
 
        transf = currentGO.transform.Find("Image");
		if (transf != null)
		{ 
			sprImagen = transf.GetComponent<Image>();
            setearImagen(this.Imagen);

		}

        transf = currentGO.transform.Find("TextoEstirable");
        if (transf != null)
        {
            txtTexto = transf.GetComponent<Text>();
            setearTexto(Texto);
        }

        transf = currentGO.transform.Find("TextoTitulo");
        if (transf != null)
        {
            txtTitulo = transf.GetComponent<Text>();
            setearTitulo(this.Titulo);
        }


        escalaZero = new Vector3(0f, 0f, 0f);
        escalaNormal = new Vector3(1, 1, 1);

        Visible = false; 
	}

    private void setearTitulo(string titulo)
    {
        txtTitulo.text = titulo;// crearTextoAleatorio();
        RectTransform rt = txtTitulo.rectTransform;
        float height = txtTitulo.GetComponent<Text>().preferredHeight;
        float width = rt.rect.width;
        titHeight = height;
        print("titHeight " + titHeight);
        titWidth = width;
    }

    private void setearImagen(Sprite sprite)
    {
		//Falta setar la imagen sprite
		RectTransform rt=sprImagen.rectTransform; 
		float height = rt.rect.height;
		float width = rt.rect.width;
		imgHeight=height;
		imgWidth=width;
    }

    private void setearTexto(string texto)
    {
        if (txtTexto != null)
        {
            txtTexto.text = texto;// crearTextoAleatorio();
            RectTransform rt = txtTexto.rectTransform;
            float height = txtTexto.GetComponent<Text>().preferredHeight;
            float width = rt.rect.width;
            txtHeight = height;
            txtWidth = width;
        }
    }
	
	// Update is called once per frame
	void Update () { 
           
		
	}

    private void actualizarBloque()
    {
        scalePanelInTimeWithDelay(this.gameObject, escalaNormal, 1f, 1.3f);
        //print("imgAlto: " + imgHeight + " txtAlto: " + txtHeight+" Suma:"+(imgHeight+txtHeight).ToString());
        this.Alto = imgHeight + txtHeight + offSet;
        setearTexto(Texto);
    }

	private void movePanel(GameObject from, GameObject to)
	{
		iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", 0.8f, "delay", 0.2f, "easetype", "easeOutExpo"));
	} 
	private void scalePanel(GameObject from, GameObject to)
	{
		iTween.ScaleTo (from, iTween.Hash ("scale",to.transform.localScale , "time", 1.0f, "easetype", "easeInQuad"));
	}
	private void scalePanelInTime(GameObject from, GameObject to, float time)
	{
		iTween.ScaleTo (from, iTween.Hash ("scale",to.transform.localScale , "time", time, "easetype", "easeInQuad"));
	}
    private void scalePanelInTime(GameObject from, Vector3 to, float time)
    {
        iTween.ScaleTo(from, iTween.Hash("scale", to, "time", time, "easetype", "spring"));
    }
    private void scalePanelInTimeWithDelay(GameObject from, Vector3 to, float time, float delay)
    {
        iTween.ScaleTo(from, iTween.Hash("scale", to, "time", time, "delay", delay, "easetype", "spring"));
    }

    private string crearTextoAleatorio()
    {
        string textoAleat="";
        int tamTexto = UnityEngine.Random.Range(20, 350);
        string strCaracteres = "& QWERTYUIOP ASDFGHJKLÑ ZXCVBNM ,.;:- qwertyuiop asdfghjklñ zxcvbnm $";
        for (int i = 0; i < tamTexto; i++)
        { 
            int nroChar=UnityEngine.Random.Range(0,strCaracteres.Length);
            string caracter=strCaracteres.Substring(nroChar,1);
            textoAleat += caracter;
        } 
        return textoAleat; 
    }

	void OnMouseDown () 
	{
		print ("toco "+this.gameObject.name);
	}
}
