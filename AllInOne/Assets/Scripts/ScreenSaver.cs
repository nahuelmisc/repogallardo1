﻿using UnityEngine;
using System.Collections;

public class ScreenSaver : MonoBehaviour {
    public static ScreenSaver Instance = null;

    void Awake()
    {
        Instance = this;
    }

	public MovieTexture movieTexture;

    private Renderer renderer; 


    #region Ejes
    private GameObject posScreenSaver;
    private GameObject posScreenSaverIzquierda; 
    #endregion

	private bool _Visible;
    public bool Visible
    {
        get { return _Visible; }
        set
        {
            _Visible = value;
            if (_Visible)
            {  
				PanelInfo.Instance.Visible=false;
				PanelMenuPrincipal.Instance.Visible=false;
                movePanelInTime(this.gameObject, posScreenSaver, 0.8f);
                BarraMenu.Instance.Visible = false;
            }
            else
            { 
                movePanelInTime(this.gameObject, posScreenSaverIzquierda, 0.8f);
                BarraMenu.Instance.Visible = true;
            }
        }
    }
	// Use this for initialization
	void Start () {
        Transform transf;

        #region BuscaEjes
        GameObject eje = GameObject.Find("Ejes");

        transf = eje.transform.Find("PosScreenSaver");
        if (transf != null)
        {
            posScreenSaver = transf.gameObject;
        }

        transf = eje.transform.Find("PosScreenSaverIzquierda");
        if (transf != null)
        {
            posScreenSaverIzquierda = transf.gameObject;
        } 
        #endregion

        /*this.GetComponent<Renderer>().material.mainTexture = movieTexture;
        movieTexture.Play();*/ 
		renderer = GetComponent<Renderer>();
		movieTexture = (MovieTexture)renderer.material.mainTexture; 
		movieTexture.loop = true;
		movieTexture.Play();

        //Visible = true;

	}
	
	// Update is called once per frame
	void OnMouseDown () {
        Visible = false;
        PanelMenuPrincipal.Instance.Visible = true;
	}

    #region AnimacionesItween
    private void movePanel(GameObject from, GameObject to)
    {
        iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", 0.8f, "delay", 0.2f, "easetype", "easeOutExpo"));
    }

    private void movePanelInTimeWithDelay(GameObject from, GameObject to, float time, float delay)
    {
        iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", time, "delay", delay, "easetype", "easeOutExpo"));
    }
    private void movePanelInTime(GameObject from, GameObject to, float time)
    {
        iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", time, "easetype", "easeOutExpo"));
    }
    #endregion
}
