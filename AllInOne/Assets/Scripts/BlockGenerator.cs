﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Institutional;
using System; 
using System.Threading;

public class BlockGenerator : MonoBehaviour {
    [SerializeField]
    private int contentNumber=0;
    private ContentHandler.InstitutionalData insd;
    public int ContentNumber
    {
        get
        {
            return contentNumber;
        }
        set
        {
            contentNumber = value;
        }
    }

	private List<GameObject> listaBloques; 
	public Transform bloquePrefab; 
	//float offSet = 9100.0f;
	private bool panelActualizado = false;
    private float maxAltoFila;
    private float maxAltoContenedor;  

	private bool _VisibleBlocks;
	
	public bool VisibleBlocks
	{
		get { return _VisibleBlocks; }
		set 
		{ 
			_VisibleBlocks = value;
            if (_VisibleBlocks)
            { 
                int indFila = 0;
                int indColumna = 0;

                maxAltoFila = 0;

                float[] altoMaxColumna = new float[6];
                for (int i = 0; i < 6; i++)
                {
                    altoMaxColumna[i] = 0;
                }


                foreach (GameObject bloque in listaBloques)
                {
                    if (indColumna == 6)
                    {
                        indColumna = 0;
                        //maxAltoContenedor += maxAltoFila;
                        maxAltoFila = 0;
                        indFila++;
                    }

                    bloque.transform.SetParent(this.transform, false);

                    if (indFila > 0)
                    {
                        bloque.transform.localPosition = new Vector3(bloque.GetComponent<PanelBloque>().Ancho * indColumna, -altoMaxColumna[indColumna], 0f);
                    }
                    else
                    {
                        bloque.transform.localPosition = new Vector3(bloque.GetComponent<PanelBloque>().Ancho * indColumna, 0f, 0f); 
                    }

                    altoMaxColumna[indColumna] += bloque.GetComponent<PanelBloque>().Alto;
                    //print("IndFila: " + indFila + " IndColumna: " + indColumna + " altoMax: " + altoMaxColumna[indColumna]);

                    indColumna++;
                }
                //Determinar un alto para el panel escrollable de manera que se ajuste al contenido
                for (int i = 0; i < 6; i++)
                {
                    if (maxAltoFila <= altoMaxColumna[i])
                    {
                        maxAltoFila = altoMaxColumna[i];
                    }
                }
                maxAltoContenedor = maxAltoFila;

                foreach (GameObject bloque in listaBloques)
                {
                    bloque.GetComponent<PanelBloque>().Visible = true;
                }

                RectTransform rt = this.GetComponent<RectTransform>();
                float ancho = rt.rect.width;
                rt.sizeDelta = new Vector2(ancho, maxAltoContenedor);

            }
            else 
            {
                foreach (GameObject bloque in listaBloques)
                {
                    bloque.GetComponent<PanelBloque>().Visible = false;
                }
            }
		
		}
	}

    private void institutionalHandler(ContentHandler.InstitutionalData insd, EventArgs e)
    {
        if ((contentNumber > insd.items.Length - 1) || (insd.items[contentNumber] == null))
        {
            Debug.Log("Not enough content loaded to fill blocks");
            return;
        }
        else
        {
            this.insd = insd;
        }

        refresh();

    }

    private void refresh()
    {
        Hashtable ht = insd.items[contentNumber].Media;

        if (ht == null)
        {
            Debug.Log("Content not found");
            return;
        }

        int cantContenidos = insd.items.Length;
        print("La cantidad de contenidos es de : "+cantContenidos);
        for (int i = 0; i < cantContenidos; i++)
        {
            Transform transfBloque = (Transform)Instantiate(bloquePrefab, new Vector3(0f, 0f, 0f), Quaternion.identity);
            GameObject oBloque = transfBloque.gameObject;
            transfBloque.gameObject.name = "PanelBloque" + (i + 1).ToString();
            oBloque.GetComponent<PanelBloque>().Texto = insd.items[i].Texto;
            oBloque.GetComponent<PanelBloque>().Titulo = insd.items[i].Titulo;
            listaBloques.Add(oBloque);
        }
        this.VisibleBlocks = true;

        /*imageList = ContentHandler.getImages(ht);
        videoList = ContentHandler.getVideos(ht);
        audioList = ContentHandler.getAudios(ht);*/
    }

	// Use this for initialization
	void Start () {



        ContentHandler.OnInstitutionalResponse += new ContentHandler.OnInstitutionalResponseHandler(this.institutionalHandler);
        listaBloques = new List<GameObject>();
        /*int cantContenidos = 23;
        
         
         for (int i=0; i<cantContenidos; i++) 
        { 
            Transform tgo = (Transform)Instantiate(bloquePrefab, new Vector3(0f, 0f, 0f), Quaternion.identity);
            GameObject go = tgo.gameObject;
            tgo.gameObject.name = "PanelBloque"+(i+1).ToString(); 
            listaBloques.Add(go); 
        }
        this.VisibleBlocks = false;*/
         
         


    }
	
	// Update is called once per frame
	void Update () {


	}
}
