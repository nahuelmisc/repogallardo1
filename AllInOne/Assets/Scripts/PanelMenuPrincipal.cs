﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelMenuPrincipal : MonoBehaviour {
    public static PanelMenuPrincipal Instance = null;

    void Awake()
    {
        Instance = this;
    }

	private GameObject currentGO;
    private GameObject lblTitulo;
	private GameObject btnPlantaBaja;
    private GameObject btnEntrePisoUno;
    private GameObject btnPrimerPiso;
    private GameObject btnEntrePisoDos;

    private bool _Visible;
    public bool Visible
    {
        get { return _Visible; }
        set
        {
            _Visible = value;
            if (_Visible)
            {
                mostrarBotones();
            }
            else
            {
                animClicBoton();
            }
        }
    }


    #region Ejes
    private GameObject posPlantaBaja;
    private GameObject posEntrePisoUno;
    private GameObject posEntrePisoDos;
    private GameObject posPrimerPiso;
    private GameObject posArribaPisos;
	private GameObject posPlantaBajaIzquierda;
	private GameObject posEntrePisoUnoIzquierda;
	private GameObject posEntrePisoDosIzquierda;
	private GameObject posPrimerPisoIzquierda;
    private GameObject posLblTitulo;
    private GameObject posLblTituloIzquierda;
    #endregion

    // Use this for initialization
	void Start () {
		Transform transf;
		currentGO = GameObject.Find(gameObject.name);

        transf = currentGO.transform.Find("lblTitulo");
        if (transf != null)
        {
            lblTitulo = transf.gameObject; 
        }

        transf = currentGO.transform.Find("btnPlantaBaja");
        if (transf != null)
        {
            btnPlantaBaja = transf.gameObject;
            btnPlantaBaja.GetComponent<Button>().onClick.AddListener(() => btnPlantaBaja_Click());
        }

        transf = currentGO.transform.Find("btnEntrePisoUno");
        if (transf != null)
        {
            btnEntrePisoUno = transf.gameObject;
            btnEntrePisoUno.GetComponent<Button>().onClick.AddListener(() => btnEntrePisoUno_Click());
        }

        transf = currentGO.transform.Find("btnPrimerPiso");
        if (transf != null)
        {
            btnPrimerPiso = transf.gameObject;
            btnPrimerPiso.GetComponent<Button>().onClick.AddListener(() => btnPrimerPiso_Click());
        }

        transf = currentGO.transform.Find("btnEntrePisoDos");
        if (transf != null)
        {
            btnEntrePisoDos = transf.gameObject;
            btnEntrePisoDos.GetComponent<Button>().onClick.AddListener(() => btnEntrePisoDos_Click());
        }

        #region BuscaEjes
        GameObject eje = GameObject.Find("Ejes");
        transf = eje.transform.Find("PosPlantaBaja");
        if (transf != null)
        {
            posPlantaBaja = transf.gameObject;
        }

        transf = eje.transform.Find("PosPrimerPiso");
        if (transf != null)
        {
            posPrimerPiso = transf.gameObject;
        }

        transf = eje.transform.Find("PosEntrePisoUno");
        if (transf != null)
        {
            posEntrePisoUno = transf.gameObject;
        }

        transf = eje.transform.Find("PosEntrePisoDos");
        if (transf != null)
        {
            posEntrePisoDos = transf.gameObject;
        }

        transf = eje.transform.Find("PosArribaPisos");
        if (transf != null)
        {
            posArribaPisos = transf.gameObject;
        }

        transf = eje.transform.Find("PosPlantaBajaIzquierda");
        if (transf != null)
        {
            posPlantaBajaIzquierda = transf.gameObject;
        }

        transf = eje.transform.Find("PosPrimerPisoIzquierda");
        if (transf != null)
        {
            posPrimerPisoIzquierda = transf.gameObject;
        }

        transf = eje.transform.Find("PosEntrePisoUnoIzquierda");
        if (transf != null)
        {
            posEntrePisoUnoIzquierda = transf.gameObject;
        }

        transf = eje.transform.Find("PosEntrePisoDosIzquierda");
        if (transf != null)
        {
            posEntrePisoDosIzquierda = transf.gameObject;
        }

        transf = eje.transform.Find("PosLblTitulo");
        if (transf != null)
        {
            posLblTitulo = transf.gameObject;
        }

        transf = eje.transform.Find("PosLblTituloIzquierda");
        if (transf != null)
        {
            posLblTituloIzquierda= transf.gameObject;
        }
        #endregion

        Visible = false;

       

	}

    private void mostrarBotones()
    {
        float time = 0.8f;
        movePanelInTimeWithDelay(btnPlantaBaja, posPlantaBaja, time, 0.8f);
        movePanelInTimeWithDelay(btnEntrePisoUno, posEntrePisoUno, time, 1.0f);
        movePanelInTimeWithDelay(btnPrimerPiso, posPrimerPiso, time, 1.2f);
        movePanelInTimeWithDelay(btnEntrePisoDos, posEntrePisoDos, time, 1.4f);
        movePanelInTimeWithDelay(lblTitulo, posLblTitulo, time, 1.6f);
    }

   
    private void animClicBoton()
    {
        float time = 0.8f;
        movePanelInTimeWithDelay(btnPlantaBaja, posPlantaBajaIzquierda, time, 0.0f);
        movePanelInTimeWithDelay(btnEntrePisoUno, posEntrePisoUnoIzquierda, time, 0.2f);
        movePanelInTimeWithDelay(btnPrimerPiso, posPrimerPisoIzquierda, time, 0.4f);
        movePanelInTimeWithDelay(btnEntrePisoDos, posEntrePisoDosIzquierda, time, 0.6f);
        movePanelInTimeWithDelay(lblTitulo, posLblTituloIzquierda, time, 0.8f);
        //Vuelven arriba
        time = 0f;
        float delay = 1.1f;
        movePanelInTimeWithDelay(btnPlantaBaja, posArribaPisos, time, delay);
        movePanelInTimeWithDelay(btnEntrePisoUno, posArribaPisos, time, delay);
        movePanelInTimeWithDelay(btnPrimerPiso, posArribaPisos, time, delay);
        movePanelInTimeWithDelay(btnEntrePisoDos, posArribaPisos, time, delay);
        movePanelInTimeWithDelay(lblTitulo, posArribaPisos, time, delay);
    }

    private void btnEntrePisoDos_Click()
    {
        Visible = false;
    }


    private void btnPrimerPiso_Click()
    {
        Visible = false;
    }

    private void btnEntrePisoUno_Click()
    {
        Visible = false;
    }

    private void btnPlantaBaja_Click()
    {
        Visible = false;
        PanelInfo.Instance.Visible = true;
    }

    #region AnimacionesItween
    private void movePanel(GameObject from, GameObject to)
    {
        iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", 0.8f, "delay", 0.2f, "easetype", "spring"));
    }

    private void movePanelInTimeWithDelay(GameObject from, GameObject to, float time, float delay)
    {
        iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", time, "delay", delay, "easetype", "spring"));
    }
    private void movePanelInTime(GameObject from, GameObject to, float time)
    {
        iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", time, "easetype", "spring"));
    }
    #endregion
    // Update is called once per frame
	void Update () {
	
	}
}
