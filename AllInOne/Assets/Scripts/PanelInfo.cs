﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PanelInfo : MonoBehaviour { 
	#region Ejes
	private GameObject posPanelBloques;
	private GameObject posPanelBloquesAbajo;
	private GameObject posLblTitulo;
	private GameObject posLblTituloIzquierda;
	private GameObject posArribaPisos;
	private GameObject posBtnVolverArriba;
	private GameObject posBtnVolver;
	#endregion
	
	public static PanelInfo Instance = null;
	
	void Awake()
	{
		Instance = this;
	}
	
	private bool _Visible; 
	public bool Visible
	{
		get { return _Visible; }
		set
		{
			_Visible = value;
			if (_Visible)
			{
				aparecerElementos(); 
			}
			else
			{
				desaparecerElementos();
				if(!ScreenSaver.Instance.Visible)
				{
					PanelMenuPrincipal.Instance.Visible = true;
				}

			}
		}
	}
	
	
	private void aparecerElementos()
	{
		movePanelInTimeWithDelay(lblTitulo, posLblTitulo, 0.8f, 1.2f);
		movePanelInTimeWithDelay(btnVolver, posBtnVolver, 0.8f, 1.3f);
		movePanelInTimeWithDelay(panelBloques, posPanelBloques, 0.0f, 1.2f);
		panelInfoGenerador.VisibleBlocks = true;
	}
	
	private void desaparecerElementos()
	{
		movePanelInTime(panelBloques, posPanelBloquesAbajo, 1.0f);
		movePanelInTimeWithDelay(btnVolver, posBtnVolverArriba, 1.0f,0.2f);
		movePanelInTimeWithDelay(lblTitulo, posArribaPisos, 1.0f,0.3f); 
	}
	
	private List<GameObject> listaBloques; 
	private float offSet = 20.0f;
	private GameObject lblTitulo;
	private GameObject btnVolver;
	private GameObject currentGO;
	private GameObject panelBloques;
	private BlockGenerator panelInfoGenerador;
	// Use this for initialization
	void Start () {
		listaBloques = new List<GameObject>();
		
		Transform transf;
		currentGO = GameObject.Find(gameObject.name);
		
		transf = currentGO.transform.Find("lblTitulo");
		if (transf != null)
		{
			lblTitulo = transf.gameObject;
		}
		
		transf = currentGO.transform.Find("btnVolver");
		if (transf != null)
		{
			btnVolver = transf.gameObject;
			btnVolver.GetComponent<Button>().onClick.AddListener(() => btnVolver_Click());
		}
		
		transf = currentGO.transform.Find("PanelScrollBloques");
		if (transf != null)
		{
			panelBloques = transf.gameObject;
		}
		
		transf = currentGO.transform.Find("PanelScrollBloques/PanelInfoGenerador");
		if (transf != null)
		{
			panelInfoGenerador = transf.GetComponent<BlockGenerator>();
		}
		
		#region BuscaEjes
		GameObject eje = GameObject.Find("Ejes");
		transf = eje.transform.Find("PosPanelBloques");
		if (transf != null)
		{
			posPanelBloques = transf.gameObject;
		}
		
		transf = eje.transform.Find("PosPanelBloquesAbajo");
		if (transf != null)
		{
			posPanelBloquesAbajo = transf.gameObject;
		}
		
		transf = eje.transform.Find("PosLblTitulo");
		if (transf != null)
		{
			posLblTitulo = transf.gameObject;
		}
		
		transf = eje.transform.Find("PosLblTituloIzquierda");
		if (transf != null)
		{
			posLblTituloIzquierda = transf.gameObject;
		}
		
		transf = eje.transform.Find("PosArribaPisos");
		if (transf != null)
		{
			posArribaPisos = transf.gameObject;
		}
		
		transf = eje.transform.Find("PosBtnVolver");
		if (transf != null)
		{
			posBtnVolver = transf.gameObject;
		}
		
		transf = eje.transform.Find("PosBtnVolverArriba");
		if (transf != null)
		{
			posBtnVolverArriba = transf.gameObject;
		}
		#endregion
		
		Visible = false;
	}
	
	private void btnVolver_Click()
	{
		Visible = false;
        panelInfoGenerador.VisibleBlocks = false;
	}
	
	#region AnimacionesItween
	private void movePanel(GameObject from, GameObject to)
	{
		iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", 0.8f, "delay", 0.2f, "easetype", "easeOutExpo"));
	}
	
	private void movePanelInTimeWithDelay(GameObject from, GameObject to, float time, float delay)
	{
		iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", time, "delay", delay, "easetype", "easeOutExpo"));
	}
	private void movePanelInTime(GameObject from, GameObject to, float time)
	{
		iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", time, "easetype", "easeOutExpo"));
	}
	#endregion
	
	// Update is called once per frame
	void Update () {
		
	}
}
