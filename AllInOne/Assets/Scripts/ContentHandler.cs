using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Promo;
using Calendar;
using FloorPlant;
using Institutional;
using System;
using System.IO;

public class ContentHandler : MonoBehaviour
{
	public static ContentHandler instance { get; private set; }

	/////////////////////////////////////////////
	/// Calendar section
	public delegate void OnCalendarResponseHandler(CalendarData cd, EventArgs e);
	public static event OnCalendarResponseHandler OnCalendarResponse;

	private static CalendarData calendarData;	
	public class CalendarItemData {
		public int		Id;
		public string	Titulo;
		public string	Resumen;
		public string	URL;
		public string	FechaActividad;
		public string	Fecha;
		public string	Texto;
		public string	BlockColor;
		public Hashtable Media;
	}
	public class CalendarData {
		public CalendarItemData[] items;
	}

	/// /////////////////////////////////////////////
	/// Promo section
	public delegate void OnPromoResponseHandler(PromoData cd, EventArgs e);
	public static event OnPromoResponseHandler OnPromoResponse;
	
	private static PromoData promoData;	
	public class PromoItemData {
		public int		Id;
		public string	Titulo;
		public string	Texto;
		public string	Imagen;
		public string	URL;
	}
	public class PromoData {
		public PromoItemData[] items;
	}

	/// /////////////////////////////////////////////
	/// FloorPlant section
	public delegate void OnFloorPlantResponseHandler(FloorPlantData cd, EventArgs e);
	public static event OnFloorPlantResponseHandler OnFloorPlantResponse;
	
	private static FloorPlantData floorPlantData;	
	public class FloorPlantItemData {
		public int		 Id;
		public string	 Titulo;
		public string	 Resumen;
		public string	 Icono;
		public string	 Imagemap;
		public string	 Texto;
		public string	 BlockColor;
		public Hashtable Media;
	}
	public class FloorPlantData {
		public FloorPlantItemData[] items;
	}

	/// /////////////////////////////////////////////
	/// Institutional section
	public delegate void OnInstitutionalResponseHandler(InstitutionalData cd, EventArgs e);
	public static event OnInstitutionalResponseHandler OnInstitutionalResponse;
	
	private static InstitutionalData institutionalData;	
	public class InstitutionalItemData {
		public int		 Id;
		public string	 Titulo;
		public string	 URL;
		public string	 Texto;
		public string	 BlockColor;
		public Hashtable Media;
	}
	public class InstitutionalData {
		public InstitutionalItemData[] items;
	}
	/////////////////////////////////////////////

	//When the object awakens, we assign the static variable
	void Awake() 
	{
		instance = this;
	}

	public static void UpdateAllContents() {
		UpdatePromoContents();
		UpdateCalendarContents ();
		UpdateInstitutionalContents ();
		UpdateFloorPlantContents ();
	}

	public static void UpdatePromoContents() {
		WebServiceHandler.getPromoService().queueServiceCall();
	}

	public static void UpdateCalendarContents() {		
		calendarData = null;
		WebServiceHandler.getCalendarService().queueServiceCall();
	}

	public static void UpdateInstitutionalContents() {		
		WebServiceHandler.getInstitutionalService ().queueServiceCall();
	}

	public static void UpdateFloorPlantContents() {		
		WebServiceHandler.getFloorPlantService().queueServiceCall();
	}

	// Use this for initialization
	void Start ()
	{
		WebServiceHandler.OnServiceResponse += new WebServiceHandler.OnServiceResponseHandler(HandleOnServiceResponse);
		UpdateAllContents ();
	}

	void HandleOnServiceResponse (ServiceHandler sh, System.EventArgs e)
	{
		if (sh.getStatus () == WSStatus.RESPONSE_OK) {
			if (sh.serviceName.Equals ("CALENDAR")) {
				CalendarStruct.CalendarWSResponse calendarResult = WebServiceHandler.getCalendarService ().getQueryResult ();

				Debug.Log ("Loading calendar data...");
				loadCalendarWebContent (calendarResult);

				if (OnCalendarResponse!=null) {
					EventArgs ea = null;
					OnCalendarResponse(calendarData, ea);
				}
			}

			if (sh.serviceName.Equals ("PROMO")) {
				PromoStruct.PromoWSResponse promoResult = WebServiceHandler.getPromoService().getQueryResult ();
				
				Debug.Log ("Loading promo data...");
				loadPromoWebContent (promoResult);

				if (OnPromoResponse!=null) {
					EventArgs ea = null;
					OnPromoResponse(promoData, ea);
				}
			}

			if (sh.serviceName.Equals ("FLOORPLANT")) {
				FloorPlantStruct.FloorPlantWSResponse floorPlantResult = WebServiceHandler.getFloorPlantService().getQueryResult ();
				
				Debug.Log ("Loading floor plant data...");
				loadFloorPlantWebContent (floorPlantResult);
				
				if (OnFloorPlantResponse!=null) {
					EventArgs ea = null;
					OnFloorPlantResponse(floorPlantData, ea);
				}
			}

			if (sh.serviceName.Equals ("INSTITUTIONAL")) {
				InstitutionalStruct.InstitutionalWSResponse institutionalResult = WebServiceHandler.getInstitutionalService().getQueryResult ();
				
				Debug.Log ("Loading institutional data...");
				loadInstitutionalWebContent (institutionalResult);
				
				if (OnInstitutionalResponse!=null) {
					EventArgs ea = null;
					OnInstitutionalResponse(institutionalData, ea);
				}
			}
		}


	}

	// Update is called once per frame
	void Update ()
	{

	}

	public static List<Media> getImages(Hashtable mediaList) {
		List<Media> list = new List<Media>();
		foreach (DictionaryEntry pair in mediaList)	{
			Media media = ((Media)pair.Value);
			if (media.type.Equals("image/jpeg") || media.type.Equals("image/png")) {
				//Debug.Log ("media.url=" + media.url);
				list.Add(media);
			}
		}
		List<Media> SortedList = list.OrderBy(o=>o.index).ToList();
		return SortedList;
	}

	public static List<Media> getVideos(Hashtable mediaList) {
		List<Media> list = new List<Media>();
		foreach (DictionaryEntry pair in mediaList)	{
			Media media = ((Media)pair.Value);
			if (media.type.Equals("video/mp4") || media.type.Equals("video/ogg")) {
				list.Add(media);
			}
		}
		List<Media> SortedList = list.OrderBy(o=>o.index).ToList();
		return SortedList;
	}

	public static List<Media> getAudios(Hashtable mediaList) {
		List<Media> list = new List<Media>();
		foreach (DictionaryEntry pair in mediaList)	{
			Media media = ((Media)pair.Value);
			if (media.type.Equals("audio/mpeg") || media.type.Equals("audio/ogg")) {
				list.Add(media);
			}
		}
		List<Media> SortedList = list.OrderBy(o=>o.index).ToList();
		return SortedList;
	}

	private void loadPromoWebContent(PromoStruct.PromoWSResponse promoResult) {
		promoData = new PromoData();
		if (promoResult.Response.datos.Length > 0) {
			promoData.items = new PromoItemData[promoResult.Response.datos.Length];
			for (int i=0; i<promoResult.Response.datos.Length; i++) {
				promoData.items[i] = new PromoItemData();
				promoData.items[i].Id = promoResult.Response.datos [i].id;
				promoData.items[i].Titulo = promoResult.Response.datos [i].titulo;
				promoData.items[i].Texto = promoResult.Response.datos [i].texto;
				promoData.items[i].Imagen = promoResult.Response.datos [i].archivo;
				promoData.items[i].URL = promoResult.Response.datos [i].url;
			}
		}
	}

	private void loadCalendarWebContent(CalendarStruct.CalendarWSResponse calendarResult) {
		calendarData = new CalendarData ();

		if (calendarResult.Response.datos.Length > 0) {

			calendarData.items = new CalendarItemData[calendarResult.Response.datos.Length];
			for (int i=0; i<calendarResult.Response.datos.Length; i++) {
				calendarData.items [i] = new CalendarItemData ();
				calendarData.items [i].Id = calendarResult.Response.datos [i].id;
				calendarData.items [i].Titulo = calendarResult.Response.datos [i].titulo;
				calendarData.items [i].Fecha = calendarResult.Response.datos [i].fecha;
				calendarData.items [i].FechaActividad = calendarResult.Response.datos [i].fechaactividad;
				calendarData.items [i].Resumen = calendarResult.Response.datos [i].resumen;
				calendarData.items [i].Texto = calendarResult.Response.datos [i].texto;
				if (calendarResult.Response.datos [i].blockcolor!=null)
					calendarData.items [i].BlockColor = calendarResult.Response.datos [i].blockcolor.Replace("#","");
				else calendarData.items [i].BlockColor = "FFFFFF";

				calendarData.items [i].Media = new Hashtable ();

				//Imagenes
				if (calendarResult.Response.datos [i].imagen != null)
					for (int j=0; j<calendarResult.Response.datos[i].imagen.Length; j++) {
						Media media = new Media ();

						media.index = calendarResult.Response.datos [i].imagen [j].index;
						media.name = calendarResult.Response.datos [i].imagen [j].titulo;
						media.id = "i" + calendarResult.Response.datos [i].imagen [j].id.ToString ();
						media.hash = calendarResult.Response.datos [i].imagen [j].hash;
						media.size = calendarResult.Response.datos [i].imagen [j].size;
						media.fileId = calendarResult.Response.datos [i].imagen[j].id;
						media.contentId = calendarResult.Response.datos [i].id;

						if (Path.GetExtension (calendarResult.Response.datos [i].imagen [j].archivo).ToLower ().Equals (".jpg")) {
								media.type = "image/jpeg";
						} else if (Path.GetExtension (calendarResult.Response.datos [i].imagen [j].archivo).ToLower ().Equals (".png")) {
								media.type = "image/png";
						}
						media.url = WebServiceHandler.selectedServer + calendarResult.Response.datos [i].imagen [j].archivo;
						media.isLocal = false;

						//Debug.Log("Adding " + media.url + "...");

						if (media.type.Length > 0)
								calendarData.items [i].Media.Add (media.id, media);
					}

				//Videos
				if (calendarResult.Response.datos [i].video != null)
					for (int j=0; j<calendarResult.Response.datos[i].video.Length; j++) {
						Media media = new Media ();

						media.index = calendarResult.Response.datos [i].video [j].index;
						media.name = calendarResult.Response.datos [i].video [j].titulo;
						media.id = "v" + calendarResult.Response.datos [i].video [j].id.ToString ();
						media.hash = calendarResult.Response.datos [i].video [j].hash;
						media.size = calendarResult.Response.datos [i].video [j].size;
						media.type = calendarResult.Response.datos [i].video [j].servicio.ToLower ();
						media.fileId = calendarResult.Response.datos [i].video[j].id;
						media.contentId = calendarResult.Response.datos [i].id;

						if (calendarResult.Response.datos [i].video [j].servicio.ToLower ().Equals ("mp4")) {
								media.type = "video/mp4";
						}
						media.url = WebServiceHandler.selectedServer + "/uploadsfotos/" + calendarResult.Response.datos [i].video [j].archivo;
						media.isLocal = false;

						if (media.type.Length > 0)
								calendarData.items [i].Media.Add (media.id, media);
					}

				//MP3
				if (calendarResult.Response.datos [i].archivo != null)
					for (int j=0; j<calendarResult.Response.datos[i].archivo.Length; j++) {
						Media media = new Media ();

						media.index = calendarResult.Response.datos [i].archivo [j].index;
						media.name = calendarResult.Response.datos [i].archivo [j].titulo;
						media.id = "a" + calendarResult.Response.datos [i].archivo [j].id.ToString ();
						media.hash = calendarResult.Response.datos [i].archivo [j].hash;
						media.size = calendarResult.Response.datos [i].archivo [j].size;
						media.fileId = calendarResult.Response.datos [i].archivo[j].id;
						media.contentId = calendarResult.Response.datos [i].id;

						if (Path.GetExtension (calendarResult.Response.datos [i].archivo [j].archivo).ToLower ().Equals (".mp3")) 
							media.type = "audio/mpeg";
						if (Path.GetExtension (calendarResult.Response.datos [i].archivo [j].archivo).ToLower ().Equals (".ogg")) 
							media.type = "audio/ogg";

						media.url = WebServiceHandler.selectedServer + calendarResult.Response.datos [i].archivo [j].archivo;
						media.isLocal = false;

						if (media.type.Length > 0)
								calendarData.items [i].Media.Add (media.id, media);
					}

				//chequea si esta en cache y reemplaza por url local
				string module = "CALENDAR";
				foreach (DictionaryEntry pair in calendarData.items[i].Media) {
					Media media = (Media)pair.Value;
					WebServiceHandler.CacheResult cr = WebServiceHandler.Instance.getCache(media.fileId, media.contentId, module, media.type, media.hash);
					string localFile = WebServiceHandler.Instance.getFileCacheName(media.fileId, media.contentId, module, media.type, media.hash);
					//Debug.Log ("Media file='" + media.url + "', cached=" + cr.cached + ", type=" + media.type);
					if (cr.cached) {
						//Debug.Log("Cache Hit! File=" + WebServiceHandler.toLocalURL(localFile) + "...");
						media.url = WebServiceHandler.toLocalURL(localFile); //use local url
						media.isLocal = true;
					}
				}
			}
		}
	}

	private void loadFloorPlantWebContent(FloorPlantStruct.FloorPlantWSResponse floorPlantResult) {
		floorPlantData = new FloorPlantData ();
	
		if (floorPlantResult.Response.datos.Length > 0) {
		
			floorPlantData.items = new FloorPlantItemData[floorPlantResult.Response.datos.Length];
			for (int i=0; i<floorPlantResult.Response.datos.Length; i++) {
				floorPlantData.items [i] = new FloorPlantItemData ();
				floorPlantData.items [i].Id = floorPlantResult.Response.datos [i].id;
				floorPlantData.items [i].Titulo = floorPlantResult.Response.datos [i].titulo;
				floorPlantData.items [i].Resumen = floorPlantResult.Response.datos [i].resumen;
				floorPlantData.items [i].Texto = floorPlantResult.Response.datos [i].texto;
				floorPlantData.items [i].Imagemap = floorPlantResult.Response.datos [i].imagemap;
				floorPlantData.items [i].Icono = floorPlantResult.Response.datos [i].icono;
				//blockcolor
				if (floorPlantResult.Response.datos [i].blockcolor!=null)
					floorPlantData.items [i].BlockColor = floorPlantResult.Response.datos [i].blockcolor.Replace("#","");
				else floorPlantData.items [i].BlockColor = "FFFFFF";

				floorPlantData.items [i].Media = new Hashtable ();
	
				//Imagenes
				if (floorPlantResult.Response.datos [i].imagen != null)
					for (int j=0; j<floorPlantResult.Response.datos[i].imagen.Length; j++) {
						Media media = new Media ();

						media.index = floorPlantResult.Response.datos [i].imagen [j].index;
						media.name = floorPlantResult.Response.datos [i].imagen [j].titulo;
						media.id = "i" + floorPlantResult.Response.datos [i].imagen [j].id.ToString ();
						media.hash = floorPlantResult.Response.datos [i].imagen [j].hash;
						media.size = floorPlantResult.Response.datos [i].imagen [j].size;
						media.fileId = floorPlantResult.Response.datos [i].imagen[j].id;
						media.contentId = floorPlantResult.Response.datos [i].id;

						if (Path.GetExtension (floorPlantResult.Response.datos [i].imagen [j].archivo).ToLower ().Equals (".jpg")) {
								media.type = "image/jpeg";
						} else if (Path.GetExtension (floorPlantResult.Response.datos [i].imagen [j].archivo).ToLower ().Equals (".png")) {
								media.type = "image/png";
						}
						media.url = WebServiceHandler.selectedServer + floorPlantResult.Response.datos [i].imagen [j].archivo;
						media.isLocal = false;

						//Debug.Log("Adding " + media.url + "...");

						if (media.type.Length > 0)
								floorPlantData.items [i].Media.Add (media.id, media);
					}
	
				//Videos
				if (floorPlantResult.Response.datos [i].video != null)
					for (int j=0; j<floorPlantResult.Response.datos[i].video.Length; j++) {
						Media media = new Media ();

						media.index = floorPlantResult.Response.datos [i].video [j].index;
						media.name = floorPlantResult.Response.datos [i].video [j].titulo;
						media.id = "v" + floorPlantResult.Response.datos [i].video [j].id.ToString ();
						media.hash = floorPlantResult.Response.datos [i].video [j].hash;
						media.size = floorPlantResult.Response.datos [i].video [j].size;
						media.type = floorPlantResult.Response.datos [i].video [j].servicio.ToLower ();
						media.fileId = floorPlantResult.Response.datos [i].video[j].id;
						media.contentId = floorPlantResult.Response.datos [i].id;

						if (floorPlantResult.Response.datos [i].video [j].servicio.ToLower ().Equals ("mp4")) {
								media.type = "video/mp4";
						}
						media.url = WebServiceHandler.selectedServer + "/uploadsfotos/" + floorPlantResult.Response.datos [i].video [j].archivo;
						media.isLocal = false;

						if (media.type.Length > 0)
								floorPlantData.items [i].Media.Add (media.id, media);
					}
	

				//chequea si esta en cache y reemplaza por url local
				string module = "FLOORPLANT";
				foreach (DictionaryEntry pair in floorPlantData.items[i].Media) {
					Media media = (Media)pair.Value;
					WebServiceHandler.CacheResult cr = WebServiceHandler.Instance.getCache(media.fileId, media.contentId, module, media.type, media.hash);
					string localFile = WebServiceHandler.Instance.getFileCacheName(media.fileId, media.contentId, module, media.type, media.hash);
					//Debug.Log ("Media file='" + media.url + "', cached=" + cr.cached + ", type=" + media.type);
					if (cr.cached) {
						//Debug.Log("Cache Hit! File=" + WebServiceHandler.toLocalURL(localFile) + "...");
						media.url = WebServiceHandler.toLocalURL(localFile); //use local url
						media.isLocal = true;
					}
				}
			}
		}
	}

	private void loadInstitutionalWebContent(InstitutionalStruct.InstitutionalWSResponse institutionalResult) {
		institutionalData = new InstitutionalData ();
		
		if (institutionalResult.Response.datos.Length > 0) {
			
			institutionalData.items = new InstitutionalItemData[institutionalResult.Response.datos.Length];
			for (int i=0; i<institutionalResult.Response.datos.Length; i++) {
				institutionalData.items [i] = new InstitutionalItemData ();
				institutionalData.items [i].Id = institutionalResult.Response.datos [i].id;
				institutionalData.items [i].Titulo = institutionalResult.Response.datos [i].titulo;
				institutionalData.items [i].Texto = institutionalResult.Response.datos [i].texto;
				institutionalData.items [i].URL = institutionalResult.Response.datos [i].url;
				if (institutionalResult.Response.datos [i].blockcolor!=null)
					institutionalData.items [i].BlockColor = institutionalResult.Response.datos [i].blockcolor.Replace("#","");
				else institutionalData.items [i].BlockColor = "FFFFFF";
				
				institutionalData.items [i].Media = new Hashtable ();
				
				//Imagenes
				if (institutionalResult.Response.datos [i].imagen != null)
				for (int j=0; j<institutionalResult.Response.datos[i].imagen.Length; j++) {
					Media media = new Media ();
					
					media.index = institutionalResult.Response.datos [i].imagen [j].index;
					media.name = institutionalResult.Response.datos [i].imagen [j].titulo;
					media.id = "i" + institutionalResult.Response.datos [i].imagen [j].id.ToString ();
					media.hash = institutionalResult.Response.datos [i].imagen [j].hash;
					media.size = institutionalResult.Response.datos [i].imagen [j].size;
					media.fileId = institutionalResult.Response.datos [i].imagen[j].id;
					media.contentId = institutionalResult.Response.datos [i].id;
					
					if (Path.GetExtension (institutionalResult.Response.datos [i].imagen [j].archivo).ToLower ().Equals (".jpg")) {
						media.type = "image/jpeg";
					} else if (Path.GetExtension (institutionalResult.Response.datos [i].imagen [j].archivo).ToLower ().Equals (".png")) {
						media.type = "image/png";
					}
					media.url = WebServiceHandler.selectedServer + institutionalResult.Response.datos [i].imagen [j].archivo;
					media.isLocal = false;
					
					//Debug.Log("Adding " + media.url + "...");
					
					if (media.type.Length > 0)
						institutionalData.items [i].Media.Add (media.id, media);
				}
				
				//Videos
				if (institutionalResult.Response.datos [i].video != null)
				for (int j=0; j<institutionalResult.Response.datos[i].video.Length; j++) {
					Media media = new Media ();
					
					media.index = institutionalResult.Response.datos [i].video [j].index;
					media.name = institutionalResult.Response.datos [i].video [j].titulo;
					media.id = "v" + institutionalResult.Response.datos [i].video [j].id.ToString ();
					media.hash = institutionalResult.Response.datos [i].video [j].hash;
					media.size = institutionalResult.Response.datos [i].video [j].size;
					media.type = institutionalResult.Response.datos [i].video [j].servicio.ToLower ();
					media.fileId = institutionalResult.Response.datos [i].video[j].id;
					media.contentId = institutionalResult.Response.datos [i].id;

					if (institutionalResult.Response.datos [i].video [j].servicio.ToLower ().Equals ("mp4")) {
						media.type = "video/mp4";
					}
					media.url = WebServiceHandler.selectedServer + "/uploadsfotos/" + institutionalResult.Response.datos [i].video [j].archivo;
					media.isLocal = false;
					
					if (media.type.Length > 0)
						institutionalData.items [i].Media.Add (media.id, media);
				}
				
				//Audio
				if (institutionalResult.Response.datos [i].archivo != null)
				for (int j=0; j<institutionalResult.Response.datos[i].archivo.Length; j++) {
					Media media = new Media ();
					
					media.index = institutionalResult.Response.datos [i].archivo [j].index;
					media.name = institutionalResult.Response.datos [i].archivo [j].titulo;
					media.id = "a" + institutionalResult.Response.datos [i].archivo [j].id.ToString ();
					media.hash = institutionalResult.Response.datos [i].archivo [j].hash;
					media.size = institutionalResult.Response.datos [i].archivo [j].size;
					media.fileId = institutionalResult.Response.datos [i].archivo[j].id;
					media.contentId = institutionalResult.Response.datos [i].id;
					
					if (Path.GetExtension (institutionalResult.Response.datos [i].archivo [j].archivo).ToLower ().Equals (".mp3")) 
						media.type = "audio/mpeg";
					if (Path.GetExtension (institutionalResult.Response.datos [i].archivo [j].archivo).ToLower ().Equals (".ogg")) 
						media.type = "audio/ogg";
					
					media.url = WebServiceHandler.selectedServer + institutionalResult.Response.datos [i].archivo [j].archivo;
					media.isLocal = false;
					
					if (media.type.Length > 0)
						institutionalData.items [i].Media.Add (media.id, media);
				}

				//chequea si esta en cache y reemplaza por url local
				string module = "INSTITUTIONAL";
				foreach (DictionaryEntry pair in institutionalData.items[i].Media) {
					Media media = (Media)pair.Value;
					WebServiceHandler.CacheResult cr = WebServiceHandler.Instance.getCache(media.fileId, media.contentId, module, media.type, media.hash);
					string localFile = WebServiceHandler.Instance.getFileCacheName(media.fileId, media.contentId, module, media.type, media.hash);
					//Debug.Log ("Media file='" + media.url + "', cached=" + cr.cached + ", type=" + media.type);
					if (cr.cached) {
						//Debug.Log("Cache Hit! File=" + WebServiceHandler.toLocalURL(localFile) + "...");
						media.url = WebServiceHandler.toLocalURL(localFile); //use local url
						media.isLocal = true;
					}
				}
			}
		}
	}
}

