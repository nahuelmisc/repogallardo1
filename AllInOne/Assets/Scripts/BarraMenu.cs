﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BarraMenu : MonoBehaviour {

    public static BarraMenu Instance = null;

    void Awake()
    {
        Instance = this;
    }

	private GameObject currentGO;
	private GameObject btnVisitarWeb;
	private GameObject btnMenu;
    #region Ejes
    private GameObject posBarraMenu;
	private GameObject posBarraMenuInvisible;
    #endregion
	private GameObject btnGSF;


    private bool _Visible;

    public bool Visible
    {
        get { return _Visible; }
        set 
        {
            _Visible = value;
            if (_Visible)
            { 
               movePanel(this.gameObject, posBarraMenu);
            }
            else
            {
                movePanel(this.gameObject, posBarraMenuInvisible);
            }
        }
    }


	// Use this for initialization
	void Start () {
		Transform transf;
		currentGO = GameObject.Find(gameObject.name);

        #region BuscaEjes
        GameObject eje = GameObject.Find("Ejes");
        transf = eje.transform.Find("PosBarraMenu");
        if (transf != null)
        {
            posBarraMenu = transf.gameObject;
        }

        transf = eje.transform.Find("PosBarraMenuInvisible");
        if (transf != null)
        {
            posBarraMenuInvisible = transf.gameObject;
        }
        #endregion

        transf = currentGO.transform.Find("btnVisitarWeb");
		if (transf != null)
		{ 
			btnVisitarWeb = transf.gameObject;
			btnVisitarWeb.GetComponent<Button>().onClick.AddListener(() => btnVisitarWeb_Click());
		}

		transf = currentGO.transform.Find("btnMenu");
		if (transf != null)
		{ 
			btnMenu = transf.gameObject;  
			btnMenu.GetComponent<Button>().onClick.AddListener(() => btnMenu_Click());
		}
	
		transf = currentGO.transform.Find("btnGSF");
		if (transf != null)
		{ 
			btnGSF = transf.gameObject;
			btnGSF.GetComponent<Button>().onClick.AddListener(() => btnGSF_Click());
		} 

		

		//Visible = true;
	}

    private void btnGSF_Click()
    {
		print ("btnGSF_Click");
        Application.OpenURL("http://www.santafe.gov.ar/"); 
    }

    private void btnMenu_Click()
    {
        ScreenSaver.Instance.Visible = true;
    }

    private void btnVisitarWeb_Click()
    {
        Application.OpenURL("http://www.museogallardo.gob.ar/");
    }

    private void movePanel(GameObject from, GameObject to)
    {
        iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", 0.8f, "delay", 0.2f, "easetype", "easeOutExpo"));
    }

    private void movePanelInTimeWithDelay(GameObject from, GameObject to, float time, float delay)
    {
        iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", time, "delay", delay, "easetype", "easeOutExpo"));
    }

    
}
